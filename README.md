# nuff-said-code-challenge-flavio

## Getting Started

Clone repository:

```bash
git clone git@gitlab.com:flavio.espinoza/nuff-said-code-challenge-flavio.git
```

CD into local project directory:

```bash
cd nuff-said-code-challenge-flavio
```

Install dependencies:

```bash
yarn install
```

## Run

Start app:

```bash
yarn run start
```

Open in browser:

http://localhost:3000/

Screenshot of application in Chrome browser:

![Screenshot](docs/img/screenshot-1.png)

## Testing

Run cypress unit-test:

```bash
yarn run test
```

Click on `App.test.js` to run front end unit-test:

![test-1](docs/img/test-1.png)

View the `unit-test` in realtime in the Cypress test runner which will open in Chrome:

![test-2](docs/img/test-2.png)