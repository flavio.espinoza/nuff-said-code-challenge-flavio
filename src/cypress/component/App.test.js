import React from "react";
import { mount } from "cypress-react-unit-test";
import App from "../../App";

describe("App", () => {
  it("renders App component and child components and test buttons", () => {
    mount(<App />);
    // use standard Cypress commands
    cy.get("#sectionMessagesGrid").should("be.visible");
    cy.get("#sectionListMessages").should("be.visible");
    cy.contains("Notification").should("be.visible");
    // TODO: Need to add snackbar test
    cy.get("#buttonStartStopMessages")
      .contains("Stop")
      .should("be.visible")
      .wait(5000)
      .click()
      .contains("Start")
      .wait(5000)
      .click()
      .contains("Stop");
    cy.get("#buttonClearAll")
      .contains("Clear")
      .should("be.visible")
      .click();
    cy.get("#titleErrors").contains("Error Type 1").should("be.visible");
    cy.get("#subTitleErrors").contains("Count").should("be.visible");
    cy.get("#titleWarnings").contains("Warning Type 2").should("be.visible");
    cy.get("#subTitleWarnings").contains("Count").should("be.visible");
    cy.get("#titleInfos").contains("Info Type 3").should("be.visible");
    cy.get("#subTitleInfos").contains("Count").should("be.visible");
  });
});
