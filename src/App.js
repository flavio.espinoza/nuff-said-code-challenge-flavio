import React from "react";
import ListMessages from "./components/ListMessages";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    error: {
      main: "#F56236",
    },
    warning: {
      main: "#FCE788",
    },
    info: {
      main: "#88FCA3",
    },
    typography: {
      fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
      fontSize: 12,
      fontWeightLight: 300,
      fontWeightRegular: 400,
      fontWeightMedium: 500,
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <ListMessages />
      </div>
    </ThemeProvider>
  );
}

export default App;