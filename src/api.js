import Chance from "chance";
import _ from "lodash";

let timer;
class MessageGenerator {
  constructor(options) {
    this.messageCallback = options.messageCallback;
    this.stopGeneration = false;
    this.chance = new Chance();
  }

  stop() {
    this.stopGeneration = true;
    this.generate(true);
  }

  start() {
    this.stopGeneration = false;
    this.generate(false);
  }

  isStarted() {
    return !this.stopGeneration;
  }

  /**
   * priority from 1 to 3, 1 = error, 2 = warning, 3 = info
   * */
  generate(stopGeneration) {
    if (!stopGeneration) {
      const id = this.chance.guid();
      const message = this.chance.first();
      const priority = _.random(1, 3);
      const nextInMS = _.random(500, 3000);
      this.messageCallback({
        id,
        message,
        priority,
      });
      timer = window.setTimeout(() => {
        this.generate();
      }, nextInMS);
    } else {
      window.clearTimeout(timer);
      return;
    }
  }
}

export default MessageGenerator;
