import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    position: 'relative',
    height: 28,
    padding: theme.spacing(2),
    marginBottom: theme.spacing(1),
    fontSize: 13,
    fontFamily: theme.typography.fontFamily,
    borderRadius: 2,
  },
  inlineTable: {
    display: "inline-table",
  },
  error: {
    backgroundColor: theme.palette.error.main,
  },
  warning: {
    backgroundColor: theme.palette.warning.main,
  },
  info: {
    backgroundColor: theme.palette.info.main,
  },
  notification: {
    position: 'absolute',
    left: 18,
    top: 16,
  },
  btnClear: {
    position: 'absolute',
    right: 32,
    top: 24,
    cursor: 'pointer',
  }
}));

function GridMessages(props) {
  const classes = useStyles();
  let elevation = 4
  return (
    <Grid id={'sectionMessagesGrid'} container spacing={1}>
      <Grid item xs>
        {props.errors.map((error) => (
          <Paper className={`${classes.paper} ${classes.error}`} key={error.id} elevation={elevation}>
            <div class={classes.notification}>Notification</div>
            <div class={classes.btnClear} onClick={() => props.removeMessage('error', error.id)}>Clear</div>
          </Paper>
        ))}
      </Grid>
      <Grid item xs>
        {props.warnings.map((warning) => (
          <Paper className={`${classes.paper} ${classes.warning}`} key={warning.id} elevation={elevation}>
            <div class={classes.notification}>Notification</div>
            <div class={classes.btnClear} onClick={() => props.removeMessage('warning', warning.id)}>Clear</div>
          </Paper>
        ))}
      </Grid>
      <Grid item xs>
        {props.infos.map((info) => (
          <Paper className={`${classes.paper} ${classes.info}`} key={info.id} elevation={elevation}>
            <div class={classes.notification}>Notification</div>
            <div class={classes.btnClear} onClick={() => props.removeMessage('info', info.id)}>Clear</div>
          </Paper>
        ))}
      </Grid>
    </Grid>
  );
}

export default GridMessages;
