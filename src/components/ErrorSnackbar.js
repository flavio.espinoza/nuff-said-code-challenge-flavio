import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import MuiAlert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  root: {
    top: 0,
    width: 400,
    color: 'black',
    borderRadius: 2,
  },
}));

function Alert(props) {
  return <MuiAlert elevation={8} variant="filled" icon={false} {...props} />;
}

function ErrorSnackbar(props) {
  const classes = useStyles();
  return (
    <Snackbar
      className={classes.root}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
      open={props.open}
      onClose={props.handleClose}
      autoHideDuration={props.autoHideDuration}
      resumeHideDuration={props.autoHideDuration}
      key={"top" + "center"}
      action={
        <React.Fragment>
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={props.handleClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        </React.Fragment>
      }
    >
      <Alert onClose={props.handleClose} severity="error" className={classes.root}>
        Error
      </Alert>
    </Snackbar>
  );
}
export default ErrorSnackbar;
