import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import GridMessages from "./GridMessages";
import ErrorSnackbar from "./ErrorSnackbar";
import Container from "@material-ui/core/Container";
import Api from "../api";
import _ from "lodash";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(7),
  },
  header: {
    textAlign: "left",
    fontFamily: theme.typography.fontFamily,
  },
  title: {
    fontSize: 20,
    fontWeight: 500,
    marginBottom: theme.spacing(0.5),
  },
  subTitle: {
    fontSize: 13,
    marginBottom: theme.spacing(1),
  },
  btnGroup: {
    borderTop: "1px solid gray",
    marginBottom: theme.spacing(6),
  },
  btnInfo: {
    width: 120,
    marginLeft: theme.spacing(1),
    backgroundColor: theme.palette.info.main,
  },
}));

function ListMessages() {
  const classes = useStyles();
  const [errors, setErrors] = useState([]);
  const [warnings, setWarnings] = useState([]);
  const [infos, setInfos] = useState([]);
  const [isApiStarted, setIsApiStarted] = useState(null);
  const [snackInfo, setSnackInfo] = useState({
    id: null,
    priority: 0,
    message: null,
  });
  const [snackAutoHide, setSnackAutoHide] = useState(200000);
  const [showSnack, setShowSnack] = useState(false);

  const api = new Api({
    messageCallback: (message) => {
      messageCallback(message);
    },
  });

  function startApi() {
    api.start();
    setIsApiStarted(api.isStarted());
  }

  function stopApi() {
    api.stop();
    setIsApiStarted(api.isStarted());
  }

  function messageClearAll() {
    setErrors(() => []);
    setWarnings(() => []);
    setInfos(() => []);
  }

  function messageCallback(message) {
    if (message.priority === 1) {
      setErrors((errors) => [message, ...errors.slice()]);
      setSnackAutoHide(2000);
      setSnackInfo(() => message);
      setShowSnack(true);
    } else if (message.priority === 2) {
      setWarnings((warnings) => [message, ...warnings.slice()]);
    } else if (message.priority === 3) {
      setInfos((infos) => [message, ...infos.slice()]);
    }
  }

  function removeMessage(type, id) {
    if (type === "error") {
      setErrors(() => [...errors.filter((item) => item.id !== id)]);
    } else if (type === "warning") {
      setWarnings(() => [...warnings.filter((item) => item.id !== id)]);
    } else if (type === "info") {
      setInfos(() => [...infos.filter((item) => item.id !== id)]);
    }
  }

  function handleSnackClose() {
    setShowSnack(false);
  }

  useEffect(() => {
    api.start();
    setIsApiStarted(api.isStarted());
  }, []);

  return (
    <Container id="sectionListMessages" maxWidth="lg">
      <div className={classes.root}>
        <Grid
          container
          spacing={1}
          justify="center"
          className={classes.btnGroup}
        >
          <Grid item>
            <Button
              id={"buttonStartStopMessages"}
              variant="contained"
              className={classes.btnInfo}
              onClick={() => {
                if (isApiStarted) {
                  return stopApi();
                } else {
                  return startApi();
                }
              }}
            >
              {isApiStarted ? "Stop" : "Start"}
            </Button>
            <Button
              id={"buttonClearAll"}
              variant="contained"
              className={classes.btnInfo}
              onClick={messageClearAll}
            >
              Clear
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={1}>
          <Grid item xs>
            <div className={classes.header}>
              <div id={"titleErrors"} className={classes.title}>
                Error Type 1
              </div>
              <div id={"subTitleErrors"} className={classes.subTitle}>
                Count {errors.length}
              </div>
            </div>
          </Grid>
          <Grid item xs>
            <div className={classes.header}>
              <div id={"titleWarnings"} className={classes.title}>
                Warning Type 2
              </div>
              <div id={"subTitleWarnings"} className={classes.subTitle}>
                Count {warnings.length}
              </div>
            </div>
          </Grid>
          <Grid item xs>
            <div className={classes.header}>
              <div id={"titleInfos"} className={classes.title}>
                Info Type 3
              </div>
              <div id={"subTitleInfos"} className={classes.subTitle}>
                Count {infos.length}
              </div>
            </div>
          </Grid>
        </Grid>
        <GridMessages
          {...{
            errors,
            warnings,
            infos,
            removeMessage,
          }}
        />
      </div>
      <ErrorSnackbar id={'errorSnackBar'}
        {...{
          open: showSnack,
          handleClose: handleSnackClose,
          autoHideDuration: snackAutoHide,
        }}
      />
    </Container>
  );
}

export default ListMessages;
